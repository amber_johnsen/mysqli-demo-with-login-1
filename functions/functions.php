<?php 

include_once('xyz.php');

////////////////////////////////////////////////////////////////
/////////              MySQLi Functions           //////////////
////////////////////////////////////////////////////////////////

function connection() {
    
    $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database ['.$conn->connect_errno.']');
    }
    
    return $conn;
}

////////////////////////////////////////////////////////////////
/////////              Show all Data              //////////////
////////////////////////////////////////////////////////////////

function get_data() {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_people";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "fname" => $row['FNAME'],
            "lname" => $row['LNAME'],
            "dob" => $row['DOB']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

function showData($data, $page) {
    
    $array = json_decode($data, True);

    $today = date("Y-m-d");
    
    $output = "";

    if (count($array) > 0 ) {
        for ($i = 0; $i < count($array); $i++) {
            
            //Calculate difference between 2 dates
            $dateDiff = two_dates($today, $array[$i]['dob']);
            
            //Display date formatted
            $showDob = date("d F Y", strtotime($array[$i]['dob']));
            
            if ($page == "index") {
                //String for HTML table code
            $output .= "<tr><td>".$array[$i]['fname']."</td><td>".$array[$i]['lname']."</td><td>".$showDob."</td><td class='age'>".$dateDiff."</td></tr>";
            }
            
            if ($page == "admin") {
                //String for HTML table code
                $output .= "<tr><td>".$array[$i]['fname']."</td><td>".$array[$i]['lname']."</td><td>".$showDob."</td><td class='age'>".$dateDiff."</td><td><a href=\"edit.php?id=".$array[$i]['id']."\">Edit</a></td><td><a href=\"delete.php?id=".$array[$i]['id']."\">Delete</a></td></tr>";    
            }   
        }
        
        return $output;
    }
    else {
        $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
        
        return $output;
    }
}



////////////////////////////////////////////////////////////////
/////////                  Edit Form              //////////////
////////////////////////////////////////////////////////////////

function loadData($id) {

    $db = connection();
    $sql = "SELECT * FROM tbl_people WHERE ID = $id";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "fname" => $row['FNAME'],
            "lname" => $row['LNAME'],
            "dob" => $row['DOB']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;        
}

//Fill in data for forms

function displayID()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['id'];
}

function displayFirstName()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['fname'];
}

function displayLastName()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['lname'];
}

function displayDob()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    $dob   = $array[0]['dob'];
    return date("Y-m-d", strtotime($dob));
}

//Saving Changes

function editPerson() {

    if(isset($_POST['updateForm']))
    {
        $db = connection();

        $fname = $db->real_escape_string($_POST['fname']);
        $lname = $db->real_escape_string($_POST['lname']);
        $dob = $db->real_escape_string($_POST['dob']);
        $id = $db->real_escape_string($_POST['id']);

        $sql = "UPDATE tbl_people SET FNAME='".$fname."', LNAME='".$lname."', DOB='".$dob."' WHERE ID = ".$id."";

        $result = $db->query($sql);

        if ($result == 1) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            return "<br><br>An Error has occured";
            exit();
        }
    }  
}

////////////////////////////////////////////////////////////////
/////////              Add Information            //////////////
////////////////////////////////////////////////////////////////

function addRecordUsingJSON($jsonString) {
    
    $array = json_decode($jsonString, TRUE);

    $db = connection();

    $fname = $db->real_escape_string($array['fname']);
    $lname = $db->real_escape_string($array['lname']);
    $dob = $db->real_escape_string($array['dob']);

    $dob2 = date("Y-m-d", strtotime($dob));

    $stmt = $db->prepare("INSERT INTO tbl_people (FNAME, LNAME, DOB) VALUES (?, ?, ?)");
    $stmt->bind_param("sss", $fname, $lname, $dob2);
    $stmt->execute();
    
    print $stmt->error; //to check errors

    $result = $stmt->affected_rows;

    $stmt->close();
    $db->close();

    if ($result > 0 ) {

        $output = "Success";
        return $output;
    }
    else {

        $output = "Error!";
        return $output;
    }
}

function addRecordUsingForm() {

    if(isset($_POST['addForm']))
    {
        $db = connection();

        $fname = $db->real_escape_string($_POST['fname']);
        $lname = $db->real_escape_string($_POST['lname']);
        $dob = $db->real_escape_string($_POST['dob']);

        $dob2 = date("Y-m-d", strtotime($dob));

        $stmt = $db->prepare("INSERT INTO tbl_people (FNAME, LNAME, DOB) VALUES (?, ?, ?)");
        $stmt->bind_param("sss", $fname, $lname, $dob2);
        $stmt->execute();
        
        print $stmt->error; //to check errors

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result > 0) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}

////////////////////////////////////////////////////////////////
/////////              Delete Record              //////////////
////////////////////////////////////////////////////////////////

function removeSingleRecord() {

    if(isset($_POST['removeRecord']))
    {
        $db = connection();

        $id = $db->real_escape_string($_POST['id']);

        $stmt = $db->prepare("DELETE FROM tbl_people WHERE ID = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        
        print $stmt->error; //to check errors

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result == 1) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}

////////////////////////////////////////////////////////////////
/////////               RESET DATA                //////////////
////////////////////////////////////////////////////////////////

function resetData() {

    if(isset($_POST['reset']))
    {
        $db = connection();

        $sql = "TRUNCATE TABLE tbl_people; ";
        $sql .= "INSERT INTO `tbl_people` (`ID`, `FNAME`, `LNAME`, `DOB`) VALUES";
        $sql .= " (1, 'Lisa', 'Matthews', '1980-11-01'),";
        $sql .= " (2, 'John', 'Doe', '1900-01-01'),";
        $sql .= " (3, 'Jane', 'Jackson', '2000-01-01');";

        $select = $db->query($sql);

        $db->close();

        logout();
    }
}

////////////////////////////////////////////////////////////////
/////////               Login Admin               //////////////
////////////////////////////////////////////////////////////////

function loginAdmin() {

    if(isset($_POST['login']))
    {
        $db = connection();

        $user = $db->real_escape_string($_POST['username']);
        $pass = $db->real_escape_string($_POST['password']);

        $sql = "SELECT * FROM tbl_users WHERE USERNAME = '$user' && PASSWRD = '$pass'";
        $arr = [];

        $result = $db->query($sql);

        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "user" => $row['USERNAME'],
                "pass" => $row['PASSWRD']
            );
        }

        $result->free();
        $db->close();

        if (($user == $arr[0]['user']) && ($pass == $arr[0]['pass']))
        {
            $_SESSION['login'] = TRUE;
            redirect("admin/index.php");
        }
        else
        {
            $_SESSION['login'] = FALSE;
            echo "<h1 class='removeSure'>Your login details are incorrect.</h1>";
        }
    }
}

////////////////////////////////////////////////////////////////
/////////              Logout Admin               //////////////
////////////////////////////////////////////////////////////////

function logout()
{
    if(isset($_POST['logout']))
    {
        session_start();

        // Unset all of the session variables.
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();

        redirect("../index.php");
    }
}

////////////////////////////////////////////////////////////////
/////////            NON MySQLi Functions         //////////////
////////////////////////////////////////////////////////////////

function two_dates($date1, $date2) {
    $diff = abs(strtotime($date2) - strtotime($date1));
    $years = floor($diff / (365*60*60*24));
    return $years;
}

function redirect($location)
{
    $URL = $location;
    echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
    echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
    exit();
}

?>