<?php include_once('functions/functions.php');
session_start();
 loginAdmin(); ?>

<!doctype html>
<html>
    <head>
        <title>People</title>
        <link rel="stylesheet" href="css/main.css" type="text/css">
    </head>
    <body>
        <?php 
        if( !isset($_SESSION['login']) )
        {
        ?>
        <div class="container">
            <h1>People</h1>
            <h2 class="left">Login Form</h2>
            <header>
                Please enter in your username and password
            </header>
            <form method='POST' >
                <input type="text" name="username" placeholder="first name">
                <input type="password" name="password" placeholder="last name">
                <input type="submit" name="login" value="submit form">
            </form>
            <h2 class="left"><a href="index.php">Cancel</a></h2>
        </div>
        <?php  
        }
        else
        {
            redirect("admin/index.php");
        }
        ?>
    </body>
</html>   