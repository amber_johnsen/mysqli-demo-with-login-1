<?php include_once('../functions/functions.php'); ?> 
<?php 
    session_start();
    editPerson(); 
?>
<!doctype html>
<html>
    <head>
        <title>People</title>
        <link rel="stylesheet" href="../css/main.css" type="text/css">
    </head>
    <body>
        <?php 
        if( $_SESSION['login'] == TRUE )
        {
        ?>
        <div class="container">

            <h1>People</h1>
            <h2 class="left">Edit Record</h2>

            <header>
                Edit Person = <?php echo displayFirstName()." ".displayLastName(); ?>
            </header>

            <form method="POST">
                <input type="hidden" name="id" value="<?php echo displayID(); ?>">
                <input type="text" name="fname" placeholder="first name" value="<?php echo displayFirstName(); ?>">
                <input type="text" name="lname" placeholder="last name" value="<?php echo displayLastName(); ?>">
                <input type="date" name="dob" placeholder="dd-mm-yyyy" value="<?php echo displayDob(); ?>">
                <input type="submit" name="updateForm" value="submit form">
            </form>

            <h2 class="left"><a href="index.php">Cancel</a></h2>
            
        </div>
        <?php  
        }
        else
        {
        ?>
        <div class="container">
            <h1>People</h1>
            <h1 class="removeSure">You do not have access to this page</h1>
            <h2><a href="../login.php">Go to the login screen</a></h2>
            <h2><a href="../index.php">Go back to the home screen</a></h2>
        </div>
        <?php
        }
        ?>
    </body>
</html>   