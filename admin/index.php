<?php   include_once('../functions/functions.php'); 
        session_start();
        logout();
        resetData();
?>
<!doctype html>
<html>
    <head>
        <title>People</title>
        <link rel="stylesheet" href="../css/main.css" type="text/css">
    </head>
    <body>
        <?php 
        if( $_SESSION['login'] == TRUE )
        {
        ?>
        <div class="container">
            <h1>People</h1>
            <h2 class="left">Display people and their age</h2>
            <table>
                <tr>
                    <th class="names">First Name</th>
                    <th class="names">Last Name</th>
                    <th>Date of Birth</th>
                    <th>Their Age</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
                <?php echo showData(get_data(), "admin"); ?>
            </table>
            <h2 class="left"><a href="../index.php">Index</a></h2>
            <h2 class="left"><a href="add.php">Add new Record</a></h2>

            <form method='POST' >
                <input type="submit" name="logout" value="logout">
                <!--<input type="submit" name="reset" value="Reset Data">-->
            </form>

        </div>
        <?php  
        }
        else
        {
        ?>
        <div class="container">
            <h1>People</h1>
            <h1 class="removeSure">You do not have access to this page</h1>
            <h2><a href="../login.php">Go to the login screen</a></h2>
            <h2><a href="../index.php">Go back to the home screen</a></h2>
        </div>
        <?php
        }
        ?>
    </body>
</html>     