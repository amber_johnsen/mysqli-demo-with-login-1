<?php include_once('functions/functions.php'); ?>
<!doctype html>
<html>
    <head>
        <title>People</title>
        <link rel="stylesheet" href="css/main.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <h1>People</h1>
            <h2 class="left">Display people and their age</h2>
            <table>
                <tr>
                    <th class="names">First Name</th>
                    <th class="names">Last Name</th>
                    <th>Date of Birth</th>
                    <th>Their Age</th>
                </tr>
                <?php echo showData(get_data(), "index"); ?>
            </table>
            <h2 class="left"><a href="login.php">Admin</a></h2>
            <h2 class="left"><a href="remote/showalldata.php">Show JSON String</a></h2>
        </div>
    </body>
</html>     